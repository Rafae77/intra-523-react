import React, {Component} from 'react';

import axios from 'axios';
import './App.css';

var nom = "";
var email = "";
var id = "";
var nomInput;

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      message: "",
      nom: "",
      email: "",
      id: ""
    }
  }

  handleNomChange(e) {
    nomInput = e.target.value;
  }

  componentDidMount() {
    axios.get('http://localhost:8888/getUserByName/' + nomInput).then(function(response) {
      nom = response.data.name;
      //setState();
      email = response.data.email;
      id = response.data.id;
      console.log(email);
      //this.messageSetter(reponse);
    }).catch(function(error) {
      console.log(error);
    });
  }

  render() {
    return (
      <div >
        <input onChange={this.handleNomChange} type="text" name="nom"/>
        <button onClick={this.componentDidMount} type="button">Trouve!</button>
        <p>Nom: {this.state.nom}</p>
        <p>Email: {this.state.email}</p>
        <p>Id: {this.state.id}</p>

      </div>
    );
  }

}

export default App;
